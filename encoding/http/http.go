package http

import (
	"encoding/json"
	"fmt"
	"mime"
	"mime/multipart"
	"net/http"
	"net/url"
	"reflect"
	"strings"

	"github.com/gorilla/schema"
	ue "gitlab.com/sirenia/util/encoding/url"
	"gitlab.com/sirenia/util/err"
	"go.uber.org/zap"
)

var schemaDecoder = schema.NewDecoder()

func init() {
	schemaDecoder.RegisterConverter(url.URL{}, urlConverter)
}

func urlConverter(input string) reflect.Value {
	u, err := url.Parse(input)
	if err != nil {
		return reflect.ValueOf(err)
	}
	return reflect.ValueOf(*u)
}

// Encoding for request/reply.
type Encoding int

// Various encodings,
const (
	URLEncoding Encoding = iota
	JSONEncoding
)

// UnmarshallingFailed indicates that the struct could not be unmarshalled.
type UnmarshallingFailed struct {
	target  interface{}
	problem error
}

func (e UnmarshallingFailed) Error() string {
	return fmt.Sprintf("Unmarshalling to %+v failed with %v.", e.target, e.problem)
}

// Code is
func (e UnmarshallingFailed) Code() int {
	return 400
}

// MethodNotAllowed is returned when the state of the context manager does not allow the method to be invoked.
type MethodNotAllowed struct {
	message string
}

func (e MethodNotAllowed) Error() string {
	return e.message
}

// Code is 405.
func (e MethodNotAllowed) Code() int {
	return 405
}

// Unmarshal content from an HTTP into the given struct.
func Unmarshal(r *http.Request, v interface{}, vars map[string]string) (Encoding, error) {
	switch r.Method {
	case http.MethodGet:
		enc := URLEncoding
		// Check if JSON is acceptable.
		if encString, ok := vars["enc"]; ok && strings.ToLower(encString) == "json" {
			enc = JSONEncoding
		} else if accepts := r.Header["Accept"]; len(accepts) > 0 {
			for _, accept := range accepts {
				if strings.Contains(strings.ToLower(accept), "json") {
					enc = JSONEncoding
					break
				}
			}
		}
		return enc, ue.Unmarshal(r.URL.Query(), v)

	case http.MethodPost, http.MethodPut, http.MethodDelete:
		// TODO use vars["enc"] to help determine encoding
		contentTypeString := ""
		contentType := ""
		var contentTypeParams map[string]string
		if contentTypes := r.Header["Content-Type"]; len(contentTypes) > 0 {
			contentTypeString = contentTypes[0]
		}
		contentType, contentTypeParams, mediaTypeErr := mime.ParseMediaType(contentTypeString)
		if mediaTypeErr != nil {
			zap.L().Warn("Parsing mediatype failed", zap.Error(mediaTypeErr))
		}

		zap.L().Debug("Content-Type after detection", zap.String("contentType", contentType))

		switch contentType {
		case "multipart/form-data":
			mr := multipart.NewReader(r.Body, contentTypeParams["boundary"])
			// 10 mb limit to form
			form, err := mr.ReadForm(int64(10 << 20))
			if err != nil {
				return URLEncoding, UnmarshallingFailed{target: v, problem: err}
			}
			err = schemaDecoder.Decode(v, form.Value)
			if err != nil {
				return URLEncoding, UnmarshallingFailed{target: v, problem: err}
			}

			// Went smoothly
			return URLEncoding, nil

		case "application/x-www-form-urlencoded":
			err := r.ParseForm()
			if err != nil {
				return URLEncoding, err
			}
			err = schemaDecoder.Decode(v, r.PostForm)
			if err != nil {
				zap.L().Warn("URL decoding failed", zap.Error(err))
				return URLEncoding, UnmarshallingFailed{target: v, problem: err}
			}

		default: // Default is application/json
			decoder := json.NewDecoder(r.Body)
			defer err.Log(r.Body.Close)
			return JSONEncoding, decoder.Decode(&v)
		}

	default:
		return JSONEncoding, MethodNotAllowed{message: fmt.Sprintf("Method %v not allowed.", r.Method)}
	}
	return URLEncoding, nil
}

// Marshal a struct using the following Encoding.
func Marshal(encoding Encoding, v interface{}) ([]byte, error) {
	switch encoding {
	case JSONEncoding:
		return json.Marshal(v)
	default: // URLEncoding
		return ue.Marshal(v)
	}
}
