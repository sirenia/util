package url

import (
	"encoding/json"
	"errors"
	"reflect"
	"strconv"
	"strings"

	u "net/url"
)

func valueAndType(v interface{}) (reflect.Value, reflect.Type) {
	value := reflect.ValueOf(v)
	switch value.Kind() {
	case reflect.Ptr, reflect.Interface:
		value = value.Elem()
	}
	typed := reflect.TypeOf(v)
	switch typed.Kind() {
	case reflect.Ptr, reflect.Interface:
		typed = typed.Elem()
	}
	return value, typed
}

// Marshal a given struct into an url-encoded string.
func Marshal(v interface{}) ([]byte, error) {
	actual, typed := valueAndType(v)
	values := u.Values{}
	for i := 0; i < actual.NumField(); i++ {
		f := actual.Field(i)
		t := typed.Field(i)
		switch f.Kind() {
		case reflect.String:
			values.Add(t.Name, f.String())
		case reflect.Int:
			values.Add(t.Name, strconv.Itoa(int(f.Int())))
		case reflect.Bool:
			if f.Bool() {
				values.Add(t.Name, "true")
			} else {
				values.Add(t.Name, "false")
			}
		default: // when in doubt, json
			data, err := json.Marshal(f.Interface())
			if err != nil {
				return nil, err
			}
			values.Add(t.Name, string(data))
		}
	}
	return []byte(values.Encode()), nil
}

// Unmarshal a structure of map[string]([]string) - form content - into a struct.
// Note: Only the first element in the []string (value of map) will be used currently.
func Unmarshal(m map[string]([]string), v interface{}) error {
	values, fields := valueAndType(v)
	if fields.Kind() != reflect.Struct {
		return errors.New("Cannot unmarshal into non-struct thing - " + fields.Kind().String())
	}

	//for i := 0; i < fields.NumField(); i++ {
	for qName, qValues := range m {
		// Case-insensitive comparison here.
		field, ok := fields.FieldByNameFunc(
			func(name string) bool {
				return strings.EqualFold(strings.ToLower(name), strings.ToLower(qName))
			})

		if ok {

			value := values.FieldByName(field.Name)

			// Only use first value if multiple are present.
			queryValue := qValues[0]
			switch value.Kind() {
			case reflect.Int, reflect.Int64, reflect.Int32:
				i, err := strconv.Atoi(queryValue)
				if err != nil {
					return err
				}
				value.SetInt(int64(i))
			case reflect.String:
				value.SetString(queryValue)
			case reflect.Bool:
				if strings.ToLower(queryValue) == "true" {
					value.SetBool(true)
				} else {
					value.SetBool(false)
				}
			case reflect.Map:
				// We'll assume key type is always a string and only switch on elem type
				switch value.Type().Elem().Kind() {

				case reflect.String: // map[string]string
					var v map[string]string

					err := json.Unmarshal([]byte(queryValue), &v)
					if err != nil {
						return err
					}

					val := reflect.ValueOf(v)
					value.Set(val)

				default: // map[string]interface{}
					var v map[string]interface{}

					err := json.Unmarshal([]byte(queryValue), &v)
					if err != nil {
						return err
					}

					val := reflect.ValueOf(v)
					value.Set(val)

				}
			}
		}
	}

	return nil
}
