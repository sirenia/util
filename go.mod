module gitlab.com/sirenia/util

go 1.16

require (
	github.com/gorilla/schema v1.0.2
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
)
