package err

import "go.uber.org/zap"

// ErrorFunc represents the function whose error you want logged.
type ErrorFunc func() error

// Log any errors returned by f.
// For use in defer to avoid having errcheck complain.
func Log(f ErrorFunc) {
	if err := f(); err != nil {
		zap.L().Warn("This happened", zap.Error(err))
	}
}
